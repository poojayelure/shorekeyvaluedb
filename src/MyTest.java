import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class MyTest {

	private static SM s_smInstance;
	HashMap<String,byte[]> key_vs_oid;
	FileOutputStream fos;
	ObjectOutputStream oos;
	
	FileInputStream fis;
	ObjectInputStream ois;
	
	
	public void setUp() {
		s_smInstance = SMFactory.getInstance();
		key_vs_oid = new HashMap<>();		
		loadKeys();
		System.out.println(key_vs_oid.size());
		
		try{
			fos = new FileOutputStream("/home/pooja/281/keyid");
			oos = new ObjectOutputStream(fos);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	//load keys and corresponding record ids
	public void loadKeys() {
		try{
		
			fis = new FileInputStream("/home/pooja/281/keyid");
			ois = new ObjectInputStream(fis);
			
			key_vs_oid = (HashMap<String,byte[]>)ois.readObject();			
			System.out.println("Size of Hashmap:: " + key_vs_oid.size());
			
			ois.close();			
			fis.close();			
		
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	}

	public void store(String key,String value) {
		try{
			
			SM.Record record = new SM.Record(value.getBytes().length);
			record.setBytes(value.getBytes());
			SM.OID oid = s_smInstance.store(record);
			
			byte[] oidByte = oid.toBytes();
			key_vs_oid.put(key, oidByte);			
			oos.writeObject(key_vs_oid);			
			oos.close();	
			
		} catch(Exception e) {
			e.printStackTrace();
		}	
	}
	
	public void fetch(String key) {
		try {
			SM.OID myOID = s_smInstance.getOID(key_vs_oid.get(key));
			SM.Record rec = s_smInstance.fetch(myOID);
			System.out.println("Record Fetched:");
			byte[] fetchedRec = rec.getBytes(0, rec.length());
			System.out.print(new String(fetchedRec));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void main(String args[]) {
		
		System.out.println("Adding Instance");
		MyTest myInstance = new MyTest();
		
		myInstance.setUp();
		myInstance.store("Pooja", "Yelure");
		myInstance.store("Pooja", "Yelure");
		myInstance.store("Pooja", "Yelure");
		myInstance.store("Pooja", "Yelure");
	
	}
}